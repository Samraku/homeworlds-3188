compile:
	go build -o "./bin/homeworlds-3188" ./src

run:
	./bin/homeworlds-3188

test: compile
	./bin/homeworlds-3188
