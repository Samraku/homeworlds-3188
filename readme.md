# Introduction

This is a project to generate random universes for Binary Homeworlds
(hereafter simply "Homeworlds")
a la chess-960.
Both players are guaranteed three different colors,
one of which will be green,
either both or neither player will have blue,
small stars will not have the same color as other small stars,
and all universes will have an equal chance to be generated
(within the precision of the random number package being used).

# Conceptual Basis

The conceptual basis for this approach is quite simple:

1. List all valid universes
2. Choose one at random

The second is trivial, so the first will be the primary topic of discussion here.
We will approach the problem by imagining that the universes are defined by three factors:
Universe Topography, Color Application, and First Player Asymmetry Resolution.
Applying each in order will yield an enumeration of all valid universes,
with the order of generation serving as an id for the second step,
an id for easy and unambiguous referal,
and a grouping of like universes near eachother.

## Universe Topography

|     | 1,1 | 2,2 | 3,3 | 1,2 | 1,3 | 2,3 |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
| 1,1 |  C  | --- | --- | --- | --- | --- |
| 2,2 |  A  |  C  | --- | --- | --- | --- |
| 3,3 |  A  |  A  |  C  | --- | --- | --- |
| 1,2 |  D  |  D  |  B  |  E  | --- | --- |
| 1,3 |  D  |  B  |  D  |  F  |  E  | --- |
| 2,3 |  B  |  D  |  D  |  F  |  F  |  E  |

Each number represents a star size in a binary system,
so "1,2" is a banker.
Smaller stars are always listed before larger stars, and
the two systems are listed in the same order as appear
in the row and column labels of the chart
(see enumeration below for clarification).

Universe Topography ignores first player,
so the upper-right half of the chart is crossed out.
Read the cells left to right and top to bottom,
saying the column system first and the row system second
(see enumeration below for clarification).

The 3 Universe Sizes are A-B (Microverse),
C-E (Small Universe), and
F (Large Universe).
The 6 Universe Shapes are represented with the letters A-F.
All 21 Universe Topographies (represented as cells above) are listed out below.
Each Universe Topography is of the form `[a1, a2, b1, b2]`
where `a1` and `a2` are the two stars (listed in increasing size) for one player, and
`b1` and `b2` are the two stars (listed in increasing size) for the other player, and
the order of the two systems is as in the row and column labels of the chart above.

```
[
	[1, 1, 1, 1],
	[1, 1, 2, 2], [2, 2, 2, 2],
	[1, 1, 3, 3], [2, 2, 3, 3], [3, 3, 3, 3],
	[1, 1, 1, 2], [2, 2, 1, 2], [3, 3, 1, 2], [1, 2, 1, 2],
	[1, 1, 1, 3], [2, 2, 1, 3], [3, 3, 1, 3], [1, 2, 1, 3], [1, 3, 1, 3],
	[1, 1, 2, 3], [2, 2, 2, 3], [3, 3, 2, 3], [1, 2, 2, 3], [1, 3, 2, 3], [2, 3, 2, 3]
]
```

## Chroma

This chart will be presented at two resolution levels:
the first showing only the 4 Color Palettes,
and the second showing all 27 Chroma
(with notation of the 9 Color Distributions).

|     | GBY | GBR | GYR |
|:---:|:---:|:---:|:---:|
| GBY |     | --- | --- |
| GBR |     |     | --- |
| GYR | --- | --- |     |

|      | BY-G | BY-B | BY-Y | BR-G | BR-B | BR-R | YR-G | YR-Y | YR-R |
|:---: |:---: |:---: |:---: |:---: |:---: |:---: |:---: |:---: |:---: |
| BY-G |  A   | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| BY-B |  B   |  A   | ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| BY-Y |  B   |  B   |  A   | ---- | ---- | ---- | ---- | ---- | ---- |
| BR-G |  C   |  D   |  D   |  F   | ---- | ---- | ---- | ---- | ---- |
| BR-B |  D   |  C   |  D   |  G   |  F   | ---- | ---- | ---- | ---- |
| BR-R |  D   |  D   |  E   |  G   |  G   |  F   | ---- | ---- | ---- |
| YR-G | ---- | ---- | ---- | ---- | ---- | ---- |  H   | ---- | ---- |
| YR-Y | ---- | ---- | ---- | ---- | ---- | ---- |  I   |  H   | ---- |
| YR-R | ---- | ---- | ---- | ---- | ---- | ---- |  I   |  I   |  H   |

All colors are listed in the order Green, Blue, Yellow, Red,
with the rationale that it is the order of importance and sequence:
Green is absolutely needed to do anything productive,
Blue is needed to get other colors (outside of capturing),
Yellow is needed to do almost anything with those ships, and
Red isn't strictly needed, though the game loses a lot without it.
The exception is for Color Allocations,
which are of the form `ab-s`
where `a` and `b` are the colors of the relevant Color Palette less Green
(in the color order listed above), and
`s` is the color of the ship.

Chroma ignores first player,
so the upper-right half of the charts are crossed out.
Only one player having no blue is forbidden,
so the relevant Chroma are crossed out.

Read the cells left to right and top to bottom,
saying the column Color Palette or Color Allocation first and
the row Color Palette or Color Allocation second
(see enumeration below for clarification).

The 9 Color Distributions are represented with the letters A-I.
All 27 Chroma (represented as cells above) are listed out below.
Each Chroma is of the form `[a1, b1, s1, a2, b2, s2]`

where `a1` and `b1` are the two colors of the binary star (in the color order listed above) and
`s1` the color of the ship for one player, and
`a2` and `b2` are the two colors of the binary star (in the color order listed above) and
`s2` the color of the ship for the other player, and
the order of the systems is as in the row and column labels of the chart above.

```
[
	[B, Y, G, B, Y, G],
	[B, Y, G, G, Y, B], [G, Y, B, G, Y, B],
	[B, Y, G, G, B, Y], [G, Y, B, G, B, Y], [G, B, Y, G, B, Y],
	[B, Y, G, B, R, G], [G, Y, B, B, R, G], [G, B, Y, B, R, G], [B, R, G, B, R, G],
	[B, Y, G, G, R, B], [G, Y, B, G, R, B], [G, B, Y, G, R, B], [B, R, G, G, R, B], [G, R, B, G, R, B],
	[B, Y, G, G, B, R], [G, Y, B, G, B, R], [G, B, Y, G, B, R], [B, R, G, G, B, R], [G, R, B, G, B, R], [G, B, R, G, B, R],
	[Y, R, G, Y, R, G],
	[Y, R, G, G, R, Y], [G, R, Y, G, R, Y],
	[Y, R, G, G, Y, R], [G, R, Y, G, Y, R], [G, Y, R, G, Y, R]
]
```

## First Player Asymmetry Resolution

TODO: (for now, see the `evaluate_chromatopography()` function in the code, which corresponds to this section of the readme)

# Definitions

Universe: The entire universe with all variables determined

Binary System Type: <- {Small Gemini, Medium Gemini, Large Gemini, Banker, Goldilocks, Fortress}

Universe Size: The distance between the homeworlds <- {Microverse, Small Universe, Large Universe}

Universe Shape: The graph of the possible jumps and series of jumps amongst five star systems: two homeworlds and one colony of each size

Universe Topography: The set of both Binary System Types of the Universe

Color Allocation: The three colors of a homeworld: two for the stars, and one for the ship

Color Palette: The set of the three colors of one and the other homeworlds of the Universe

Color Distribution: Mostly just freehanded what made sense to group; analogue is Universe Shape

Chroma: The set of both Color Allocations of the Universe

Chromatopography: The set of both the Universe Topography and Chroma of the Universe
