package main

import (
	"fmt"
	"math/rand"
	"time"
)

type Pyramid string

func main() {
	// initialize rng
	rng := rand.New(rand.NewSource(time.Now().UnixNano()))

	/*
		var topographies = [21][4]int{
			{1, 1, 1, 1},
			{1, 1, 2, 2}, {2, 2, 2, 2},
			{1, 1, 3, 3}, {2, 2, 3, 3}, {3, 3, 3, 3},
			{1, 1, 1, 2}, {2, 2, 1, 2}, {3, 3, 1, 2}, {1, 2, 1, 2},
			{1, 1, 1, 3}, {2, 2, 1, 3}, {3, 3, 1, 3}, {1, 2, 1, 3}, {1, 3, 1, 3},
			{1, 1, 2, 3}, {2, 2, 2, 3}, {3, 3, 2, 3}, {1, 2, 2, 3}, {1, 3, 2, 3}, {2, 3, 2, 3},
		}
	*/
	var topographies = [21][4]int{
		{1, 1, 2, 2}, {1, 1, 3, 3}, {2, 2, 3, 3}, // Universe Shape A
		{3, 3, 1, 2}, {2, 2, 1, 3}, {1, 1, 2, 3}, // Universe Shape B
		{1, 1, 1, 1}, {2, 2, 2, 2}, {3, 3, 3, 3}, // Universe Shape C
		{1, 1, 1, 2}, {2, 2, 1, 2}, {1, 1, 1, 3}, {3, 3, 1, 3}, {2, 2, 2, 3}, {3, 3, 2, 3}, // Universe Shape D
		{1, 2, 1, 2}, {1, 3, 1, 3}, {2, 3, 2, 3}, // Universe Shape E
		{1, 2, 1, 3}, {1, 2, 2, 3}, {1, 3, 2, 3}, // Universe Shape F
	}

	/*
		var chroma = [27][6]string{
			{"B", "Y", "G", "B", "Y", "G"},
			{"B", "Y", "G", "G", "Y", "B"}, {"G", "Y", "B", "G", "Y", "B"},
			{"B", "Y", "G", "G", "B", "Y"}, {"G", "Y", "B", "G", "B", "Y"}, {"G", "B", "Y", "G", "B", "Y"},
			{"B", "Y", "G", "B", "R", "G"}, {"G", "Y", "B", "B", "R", "G"}, {"G", "B", "Y", "B", "R", "G"}, {"B", "R", "G", "B", "R", "G"},
			{"B", "Y", "G", "G", "R", "B"}, {"G", "Y", "B", "G", "R", "B"}, {"G", "B", "Y", "G", "R", "B"}, {"B", "R", "G", "G", "R", "B"}, {"G", "R", "B", "G", "R", "B"},
			{"B", "Y", "G", "G", "B", "R"}, {"G", "Y", "B", "G", "B", "R"}, {"G", "B", "Y", "G", "B", "R"}, {"B", "R", "G", "G", "B", "R"}, {"G", "R", "B", "G", "B", "R"}, {"G", "B", "R", "G", "B", "R"},
			{"Y", "R", "G", "Y", "R", "G"},
			{"Y", "R", "G", "G", "R", "Y"}, {"G", "R", "Y", "G", "R", "Y"},
			{"Y", "R", "G", "G", "Y", "R"}, {"G", "R", "Y", "G", "Y", "R"}, {"G", "Y", "R", "G", "Y", "R"},
		}
	*/
	var chroma = [27][6]string{
		{"B", "Y", "G", "B", "Y", "G"}, {"G", "Y", "B", "G", "Y", "B"}, {"G", "B", "Y", "G", "B", "Y"}, // Color Distribution A (same ship color)
		{"B", "Y", "G", "G", "Y", "B"}, {"B", "Y", "G", "G", "B", "Y"}, {"G", "Y", "B", "G", "B", "Y"}, // Color Distribution B

		{"B", "Y", "G", "B", "R", "G"}, {"G", "Y", "B", "G", "R", "B"}, // Color Distribution C (same ship color)
		{"G", "Y", "B", "B", "R", "G"}, {"G", "B", "Y", "B", "R", "G"}, {"B", "Y", "G", "G", "R", "B"}, {"G", "B", "Y", "G", "R", "B"}, {"B", "Y", "G", "G", "B", "R"}, {"G", "Y", "B", "G", "B", "R"}, // Color Distribution D
		{"G", "B", "Y", "G", "B", "R"}, // Color Distribution E (same star colors)

		{"B", "R", "G", "B", "R", "G"}, {"G", "R", "B", "G", "R", "B"}, {"G", "B", "R", "G", "B", "R"}, // Color Distribution F (same ship color)
		{"B", "R", "G", "G", "R", "B"}, {"B", "R", "G", "G", "B", "R"}, {"G", "R", "B", "G", "B", "R"}, // Color Distribution G

		{"Y", "R", "G", "Y", "R", "G"}, {"G", "R", "Y", "G", "R", "Y"}, {"G", "Y", "R", "G", "Y", "R"}, // Color Distribution H (same ship color)
		{"Y", "R", "G", "G", "R", "Y"}, {"Y", "R", "G", "G", "Y", "R"}, {"G", "R", "Y", "G", "Y", "R"}, // Color Distribution I
	}

	var universes []Universe
	id := 0 // assigns ids to each universe as it is generated; should equal the index in universes

	for _, t := range topographies {
		for _, c := range chroma {
			new_universes := evaluate_chromatopography(t, c, &id)
			universes = append(universes, new_universes...)
		}
	}

	// choose random universe in universes
	fmt.Println(len(universes), "universes were generated; choosing one...")
	var generated_universe Universe
	if len(universes) != 0 {
		generated_universe = universes[rng.Intn(len(universes))]
		fmt.Println(generated_universe) // output
	} else {
		fmt.Println("Error: No universes were generated.")
	}
}

func evaluate_chromatopography(topography [4]int, chroma [6]string, id *int) []Universe {
	var new_universes []Universe

	var chromatopography_reflections uint8 // bitmap to store the reflections which double the number of universes in the chromatopography
	// A, B, D or F Universe Shape (different Binary System Types)
	if !((topography[0] == topography[2]) &&
		(topography[1] == topography[3])) {
		chromatopography_reflections += 0b1000
	}
	// D-I Color Distribution (different Color Allocations)
	if !((chroma[0] == chroma[3]) &&
		(chroma[1] == chroma[4]) &&
		(chroma[2] == chroma[5])) {
		chromatopography_reflections += 0b0100
	}
	// topography[:2] is non-gemini
	if !(topography[0] == topography[1]) {
		chromatopography_reflections += 0b0010
	}
	// topography[2:] is non-gemini
	if !(topography[2] == topography[3]) {
		chromatopography_reflections += 0b0001
	}

	for i := uint8(0); i < 16; i++ {
		// if any bit of i == 1 and the corresponding bit of reflections == 0, continue for loop
		// else i represents all reflections to be applied (since reflections will contain a 1 there)
		// if any bit of i == 0, that reflection is not applied
		if !((i & ^chromatopography_reflections) == 0b0000) {
			continue
		}
		universe_reflections := i

		// unreflected universe
		var new_universe = Universe{
			hw1: Homeworld{
				star1: Piece{
					Color: Color(chroma[0]),
					Size:  Size(topography[0]),
				},
				star2: Piece{
					Color: Color(chroma[1]),
					Size:  Size(topography[1]),
				},
				ship: Piece{
					Color: Color(chroma[2]),
					Size:  Size(3),
				},
			},
			hw2: Homeworld{
				star1: Piece{
					Color: Color(chroma[3]),
					Size:  Size(topography[2]),
				},
				star2: Piece{
					Color: Color(chroma[4]),
					Size:  Size(topography[3]),
				},
				ship: Piece{
					Color: Color(chroma[5]),
					Size:  Size(3),
				},
			},
			id: *id,
		}

		// A, B, D, or F Universe Shape: reflect topography across players
		if (universe_reflections & 0b1000) == 0b1000 {
			new_universe.hw1.star1.Size, new_universe.hw2.star1.Size = new_universe.hw2.star1.Size, new_universe.hw1.star1.Size
			new_universe.hw1.star2.Size, new_universe.hw2.star2.Size = new_universe.hw2.star2.Size, new_universe.hw1.star2.Size
			// swap last two bits of applied_reflections, to keep alignment with the reflected topography
			universe_reflections = (universe_reflections & 0b1100) | ((universe_reflections & 0b0001) << 1) | ((universe_reflections & 0b0010) >> 1)
		}
		// D-I Color Distribution (different Color Allocations): reflect Color Distribution across players
		if (universe_reflections & 0b0100) == 0b0100 {
			new_universe.hw1.star1.Color, new_universe.hw2.star1.Color = new_universe.hw2.star1.Color, new_universe.hw1.star1.Color
			new_universe.hw1.star2.Color, new_universe.hw2.star2.Color = new_universe.hw2.star2.Color, new_universe.hw1.star2.Color
			new_universe.hw1.ship.Color, new_universe.hw2.ship.Color = new_universe.hw2.ship.Color, new_universe.hw1.ship.Color
		}
		// topography[:2] is non-gemini: reflect Chroma across stars of hw1
		if (universe_reflections & 0b0010) == 0b0010 {
			new_universe.hw1.star1.Color, new_universe.hw1.star2.Color = new_universe.hw1.star2.Color, new_universe.hw1.star1.Color
		}
		// topography[2:] is non-gemini: reflect Chroma across stars of hw2
		if (universe_reflections & 0b0001) == 0b0001 {
			new_universe.hw2.star1.Color, new_universe.hw2.star2.Color = new_universe.hw2.star2.Color, new_universe.hw2.star1.Color
		}

		// continue if duplicate small stars
		if new_universe.hw1.star1.Size == 1 {
			if (new_universe.hw1.star1 == new_universe.hw2.star1) ||
				(new_universe.hw1.star1 == new_universe.hw2.star2) {
				continue
			}
		}
		if new_universe.hw1.star2.Size == 1 {
			if (new_universe.hw1.star2 == new_universe.hw2.star1) ||
				(new_universe.hw1.star2 == new_universe.hw2.star2) {
				continue
			}
		}

		new_universes = append(new_universes, new_universe)
		*id++
	}

	return new_universes
}
