package main

import (
	"strconv"
)

type Universe struct {
	hw1 Homeworld
	hw2 Homeworld
	id  int
}

type Homeworld struct {
	star1 Piece
	star2 Piece
	ship  Piece
}

type Piece struct {
	Color Color
	Size  Size
}

type Color string
type Size int

// TODO: functions on Universe:
// get Universe Size
// get Universe Shape
// get Universe Topography
// get Universe Color Palette
// get Universe Color Distribution
// get Universe Chroma
// get move notation (eg homeworld g3 b2 r3)

func (u Universe) String() string {
	var ret string

	ret += "id: " + strconv.Itoa(u.id) + "\n"
	ret += "first player:  " + u.hw1.String() + "\n"
	ret += "second player: " + u.hw2.String()

	return ret
}

// TODO: functions on Homeworld:
// get star system type (small gemini, fortress, &c.) of a Homeworld

func (h Homeworld) String() string {
	return h.star1.String() + " " + h.star2.String() + " " + h.ship.String()
}

func (p Piece) String() string {
	return string(p.Color) + strconv.Itoa(int(p.Size))
}
